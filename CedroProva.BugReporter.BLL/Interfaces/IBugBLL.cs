﻿using CedroProva.BugReporter.DAL.DataObject;
using CedroProva.BugReporter.DAL.Model;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace CedroProva.BugReporter.BLL.Interfaces
{
    public interface IBugBLL
    {
        Bug CreateBug(BugDAO BugDAO);
        void DeleteBug(string id);
        List<Bug> FindAllBugs();
        Bug FindBug(string id);
        Bug UpdateBug(string id, BugDAO BugDAO);
    }
}

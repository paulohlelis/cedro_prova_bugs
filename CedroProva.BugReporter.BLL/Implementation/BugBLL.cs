﻿using CedroProva.BugReporter.BLL.Interfaces;
using CedroProva.BugReporter.DAL.DataObject;
using CedroProva.BugReporter.DAL.Model;
using CedroProva.BugReporter.DAL.Repository;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace CedroProva.BugReporter.BLL.Implementation
{
    public class BugBLL : IBugBLL
    {
        private IBugRepository _bugRepository;

        public BugBLL()
        {
            _bugRepository = new BugRepository();
        }

        public Bug CreateBug(BugDAO bugDAO)
        {
            var newBug = new Bug(bugDAO);
            return _bugRepository.Insert(newBug);
        }

        public void DeleteBug(string id)
        {
            if (id == null)
            {
                throw new Exception("Id Cannot be null");
            }
            _bugRepository.Remove(id);
        }

        public List<Bug> FindAllBugs()
        {
            return _bugRepository.GetAll();
        }

        public Bug FindBug(string id)
        {
            if (id == null)
            {
                throw new Exception("Id Cannot be null");
            }
            return _bugRepository.GetById(id);
        }

        public Bug UpdateBug(string id, BugDAO bugDAO)
        {
            if (id == null)
            {
                throw new Exception("Id cannot be null.");
            }

            var existingBug = FindBug(id);

            if (existingBug == null)
            {
                throw new Exception("Bug does not exist.");
            }

            return _bugRepository.Update(id, new Bug(bugDAO));
        }
    }
}

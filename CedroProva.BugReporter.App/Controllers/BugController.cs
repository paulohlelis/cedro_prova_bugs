﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CedroProva.BugReporter.BLL.Implementation;
using CedroProva.BugReporter.BLL.Interfaces;
using CedroProva.BugReporter.DAL.DataObject;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace CedroProva.BugReporter.App.Controllers
{
    [Route("api/[controller]")]
    public class BugController : Controller
    {
        private IBugBLL bugBLL;
        public BugController()
        {
            bugBLL = new BugBLL();
        }
        // GET api/values
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Json(bugBLL.FindAllBugs());
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Error = ex.Message });
            }
            
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return StatusCode(522, new { Error = "Missing ID." });
                }
                var bug = bugBLL.FindBug(id);
                if(bug == null)
                {
                    return NotFound(new { Error = "Bug doest not exist." });
                }
                return Json(bug);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Error = ex.Message });
            }
            
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]BugDAO bugDAO)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return StatusCode(500, new { Error = "Invalid model." });
                }
                var inserted = bugBLL.CreateBug(bugDAO);
                return StatusCode(201, inserted);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Error = ex.Message });
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

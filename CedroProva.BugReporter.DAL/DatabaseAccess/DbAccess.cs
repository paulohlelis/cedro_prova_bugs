﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using System.Configuration;

namespace CedroProva.BugReporter.DAL.DatabaseAccess
{
    public static class DbAccess
    {
        public static string Database = "BugsDB1";
        public static string ConnectionString = "mongodb://localhost:32768";

        public static IMongoDatabase GetConnection()
        {
            MongoClient client = new MongoClient(ConnectionString);
            IMongoDatabase dbconn = client.GetDatabase(Database);
            return dbconn;
        } 
    }
}

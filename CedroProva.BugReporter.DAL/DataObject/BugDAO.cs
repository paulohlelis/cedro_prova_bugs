﻿using CedroProva.BugReporter.DAL.Enums;
using CedroProva.BugReporter.DAL.Model;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CedroProva.BugReporter.DAL.DataObject
{
    public class BugDAO
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public Severity Severity { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public string Email { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CedroProva.BugReporter.DAL.Enums
{
    public enum Severity
    {
        CRITICAL,
        MAJOR,
        MODERATE,
        MINOR,
        COSMETIC
    }
}

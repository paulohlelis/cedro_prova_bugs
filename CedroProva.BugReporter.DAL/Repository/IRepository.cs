﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CedroProva.BugReporter.DAL.Repository
{
    public interface IRepository <T> where T : class
    {
        T GetById(string id);
        List<T> GetAll();
        void Remove(string id);
        T Insert(T entity);
        T Update(string id, T entity);
    }
}

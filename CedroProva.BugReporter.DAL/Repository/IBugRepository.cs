﻿using CedroProva.BugReporter.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace CedroProva.BugReporter.DAL.Repository
{
    public interface IBugRepository : IRepository<Bug>
    {
        void AddComentario();
    }
}

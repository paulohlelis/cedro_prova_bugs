﻿using System;
using System.Collections.Generic;
using System.Text;
using CedroProva.BugReporter.DAL.DatabaseAccess;
using MongoDB.Bson;
using System.Linq;
using MongoDB.Driver;

namespace CedroProva.BugReporter.DAL.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        public List<T> GetAll()
        {
            var conn = DbAccess.GetConnection().GetCollection<T>(nameof(T)) ;
            return conn.Find(_ => true).ToList();
        }

        public T GetById(string id)
        {
            var conn = DbAccess.GetConnection().GetCollection<T>(nameof(T));
            var filter = Builders<T>.Filter.Eq("_id", id);
            return conn.Find(filter).FirstOrDefault();
        }

        public T Insert(T entity)
        {
            var conn = DbAccess.GetConnection().GetCollection<T>(nameof(T));
            conn.InsertOne(entity);
            return entity;
        }

        public void Remove(string id)
        {
            var conn = DbAccess.GetConnection().GetCollection<T>(nameof(T));
            var filter = Builders<T>.Filter.Eq("_id", id);
            conn.FindOneAndDelete(filter);
        }

        public T Update(string id, T entity)
        {
            var conn = DbAccess.GetConnection().GetCollection<T>(nameof(T));
            var filter = Builders<T>.Filter.Eq("_id", id);
            conn.FindOneAndReplace(filter, entity);
            return entity;
        }
    }
}

using CedroProva.BugReporter.DAL.DataObject;
using CedroProva.BugReporter.DAL.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace CedroProva.BugReporter.DAL.Model
{
    public class Bug
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public Severity Severity { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public DateTime Timestamp { get; set; }
        [Required]
        public string Email { get; set; }

        public Bug() { }
        public Bug(BugDAO bugDAO)
        {
            Timestamp = DateTime.Now;
            Title = bugDAO.Title;
            Severity = bugDAO.Severity;
            Description = bugDAO.Description;
            ProductId = bugDAO.ProductId;
            Email = bugDAO.Email;
        }
    }
}